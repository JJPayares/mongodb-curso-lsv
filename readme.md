# MongoDB LSV

MongoDb LSV es un repositorio que se usara para el curso del Car - Clonado por Juan Jose Payares Trocha

### Requeriments

 - Docker
 - docker-compose

 - **Iniciar el Mongo:**
```sh
$ git clone https://gitlab.com/valentinc94/mongodb-curso-lsv.git
$ docker-compose up
```

 - **Contenedor docker:**
```sh
$ docker exec -it docker_id /bin/bash
```


```sh
$ mongo
```
