from bson.objectid import ObjectId  # noqa
from car.databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="car_lsv"))
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="documents_types"
    )
)
"""print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record_query={"_id": ObjectId("62a77cf8fb2b183d4bee7084")},
        record_new_value={"label": "Número de Identificación Tributario"},
    )
)"""
"""print(
    mongo_lsv.delete_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record_id="62a77db856ddffddd49eb13d",
    )
)"""
"""print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record={"label": "Numero de identificacion Tributario", "value": "NIT"},
    )
)"""
