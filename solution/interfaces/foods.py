from dataclasses import asdict, dataclass
from datetime import datetime


@dataclass
class FoodsTypes:
    uuid: str
    name: str

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class foods:
    uuid: str  # UUID
    name: str
    healthy: str
    food_type: FoodsTypes

    def to_dict(self) -> dict:
        return asdict(self)

    @property
    def get_food_type(self):
        return self.food_type.name
