from bson.objectid import ObjectId  # noqa
from databases.client import MongoLsv

mongo_instance = MongoLsv()

"""print(
    mongo_instance.create_new_record_in_collection(
        db_name="car_lsv",
        collection="foods_types",
        record={"name": "Verduras"},
    )
)

print(
    mongo_instance.get_records_from_collection(
        db_name="car_lsv", collection="foods_types"
    )
)"""

"""print(
    mongo_instance.create_new_record_in_collection(
        db_name="car_lsv",
        collection="foods",
        record={"name": "Lechuga", "healthy": "si", "food_type": "62acd4c6932ac14678cfbf13", },
    )
)

print(
    mongo_instance.get_records_from_collection(
        db_name="car_lsv", collection="foods"
    )
)"""


foods_types = [{"name": "Verduras"}, {"name": "Frutas"}, {"name": "Granos"}, {"name": "Lacteos"},
               {"name": "Proteinas"}, ]

foods = [{"name": "Lechuga", "healthy": "si", "food_type": "62acd4c6932ac14678cfbf13", },
         {"name": "Mango", "healthy": "si", "food_type": "62acd4c6932ac14678cfbf14", },
         {"name": "Frijol", "healthy": "si", "food_type": "62acd4c6932ac14678cfbf15", },
         {"name": "Queso", "healthy": "si", "food_type": "62acd4c6932ac14678cfbf16", },
         {"name": "Pescado", "healthy": "si", "food_type": "62acd4c6932ac14678cfbf17", }]
