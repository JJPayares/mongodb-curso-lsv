from bson.objectid import ObjectId  # noqa
from databases.client import MongoLsv

mongo_instance = MongoLsv()

print(
    mongo_instance.delete_record_in_collection(
        db_name="car_lsv",
        collection="foods_types",
        record_id="62acd4c6932ac14678cfbf17",  # Proteina -> Comida Chatarra -> Delete
    )
)

print(
    mongo_instance.delete_record_in_collection(
        db_name="car_lsv",
        collection="foods",
        record_id="62acd7a6649369a70935475b",   # Pescado -> Picada -> Delete
    )
)

print(
    mongo_instance.get_records_from_collection(
        db_name="car_lsv", collection="foods_types"
    )
)

print(
    mongo_instance.get_records_from_collection(
        db_name="car_lsv", collection="foods"
    )
)
